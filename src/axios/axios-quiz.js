import axios from 'axios';

export default axios.create({
  baseURL: 'https://react-quiz-6a676.firebaseio.com/'
})